<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2/13/2017
 * Time: 4:55 PM
 */

require_once('app.php');

header('Content-Type: application/json');

$app = new App();

$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$data = file_get_contents('php://input');
$verified = $app->verifyWebhook($data, $hmac_header);
error_log('Webhook verified: '.var_export($verified, true));

if(!$verified)
    http_response_code(404);
else {
    ob_start();
    $data = json_decode($data, true);
    $order = $app->shopify->Order($data['id'])->get();
    http_response_code(200); //set 200 response code to stop retry mechanism
    $size = ob_get_length();
    header("Content-Encoding: none");
    header("Content-Length: {$size}");
    header("Connection: close");
    ob_end_flush();
    ob_flush();
    flush();
    if(session_id()) session_write_close();

    if ($order) {
        file_put_contents('ORDERLOGS.txt', date("Y-m-d H:i:s").": Sending Order " . $order['id'] . " to BlueLink\n", LOCK_EX | FILE_APPEND);
        $app->addSalesOrderToBlueLink($order['id']);
    }
}