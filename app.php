<?php
require_once('vendor/autoload.php');
require_once('BlueLink/BlueLink_Config.php');
require_once('BlueLink/LizardLink.php');
require_once('BlueLink/InventoryService.php');
require_once('BlueLink/CustomerService.php');
require_once('BlueLink/OrderService.php');

use PHPShopify\ShopifySDK as Shopify;

class App {

    public $api_token = '761d3b3f1fc0e0296de220fe9ea26198f7e35973';
    protected $states = array(
        'Alabama'=>'AL',
        'Alaska'=>'AK',
        'Arizona'=>'AZ',
        'Arkansas'=>'AR',
        'California'=>'CA',
        'Colorado'=>'CO',
        'Connecticut'=>'CT',
        'Delaware'=>'DE',
        'Florida'=>'FL',
        'Georgia'=>'GA',
        'Hawaii'=>'HI',
        'Idaho'=>'ID',
        'Illinois'=>'IL',
        'Indiana'=>'IN',
        'Iowa'=>'IA',
        'Kansas'=>'KS',
        'Kentucky'=>'KY',
        'Louisiana'=>'LA',
        'Maine'=>'ME',
        'Maryland'=>'MD',
        'Massachusetts'=>'MA',
        'Michigan'=>'MI',
        'Minnesota'=>'MN',
        'Mississippi'=>'MS',
        'Missouri'=>'MO',
        'Montana'=>'MT',
        'Nebraska'=>'NE',
        'Nevada'=>'NV',
        'New Hampshire'=>'NH',
        'New Jersey'=>'NJ',
        'New Mexico'=>'NM',
        'New York'=>'NY',
        'North Carolina'=>'NC',
        'North Dakota'=>'ND',
        'Ohio'=>'OH',
        'Oklahoma'=>'OK',
        'Oregon'=>'OR',
        'Pennsylvania'=>'PA',
        'Rhode Island'=>'RI',
        'South Carolina'=>'SC',
        'South Dakota'=>'SD',
        'Tennessee'=>'TN',
        'Texas'=>'TX',
        'Utah'=>'UT',
        'Vermont'=>'VT',
        'Virginia'=>'VA',
        'Washington'=>'WA',
        'West Virginia'=>'WV',
        'Wisconsin'=>'WI',
        'Wyoming'=>'WY'
    );

    public function __construct()
    {

        $config = array(
            'ShopUrl' => 'broke-dick-juice.myshopify.com',
            'ApiKey' => '240d580614b387325281fa30ded3b73d',
            'Password' => '837c412136cf59d19be86056ccbd65e1',
        );

        $this->shopify = new Shopify($config);
        
        $this->hookSecret = '6f04bb7376fdbf5a95eebfc4ac5b4f9b86163394c4cc43bf0e3100eea46366dd';

    }

    public function inventoryService()
    {
        return new InventoryService();
    }

    public function customerService()
    {
        return new CustomerService();
    }

    public function orderService()
    {
        return new OrderService();
    }

    public function getProducts($filter = null)
    {
        if($filter == null)
        {
            return $this->shopify->Product->get();
        }
        else
        {
            if(!is_array($filter))
            {
                echo "Error: Filter must be of type Array".PHP_EOL;
                return false;
            }
            else
            {
                return $this->shopify->Product->get($filter);
            }
        }
    }

    public function updateProductInventory($productId)
    {
        $product = $this->shopify->Product($productId)->get();
        if($product)
        {
            if(count($product['variants']) < 1)
            {
                echo 'No variants for '.$product['sku'].'</br>'.PHP_EOL;
                return false;
            }
            foreach($product['variants'] as $variant)
            {

                echo $variant['sku'].': '.$variant['inventory_quantity'].'</br>'.PHP_EOL;
                $inventory = $this->inventoryService()->getProductInventory($variant['sku']);
                if($inventory != $variant['inventory_quantity'] && is_numeric($inventory))
                {
                    $variant['inventory_quantity'] = (int) $inventory;
                    $this->shopify->Product($productId)->Variant($variant['id'])->put($variant);
                    echo 'Now: '.$variant['inventory_quantity'].'</br>'.PHP_EOL;
                }
            }
        }
        return true;
    }

    public function getCustomer($customerId)
    {
        return $this->shopify->Customer($customerId)->get();
    }

    public function addCustomerShipToAddress($orderId)
    {
        $customerService = $this->customerService();

        $order = $this->shopify->Order($orderId)->get();

        $address = $order['shipping_address'];
        if($address['province'] == 'Florida') {
            $blueLinkCustomerId = 'BrokeDickT';
            $taxAuthority = 'FL';
        }
        else {
            $blueLinkCustomerId = 'BrokeDick';
            $taxAuthority = 'XX';
        }
        $customerService->customerAddShipTo(
            $blueLinkCustomerId,
            'BD'.substr($order['customer']['default_address']['id'],-4),
            $address['first_name'].' '.$address['last_name'],
            mb_strimwidth($address['address1'],0,29),
            $address['address2'],
            $address['city'],
            $address['province_code'],
            $address['zip'],
            $address['country_code'],
            $address['first_name'].' '.$address['last_name'],
            $address['phone'],
            '',
            $taxAuthority
        );
        file_put_contents('ORDERLOGS.txt', date("Y-m-d H:i:s").": ShipTo added for order ".$orderId."\n",LOCK_EX | FILE_APPEND);

        return true;
    }

    public function getBlueLinkOrder($blueLinkOrderId)
    {
        $orderservice = $this->orderService();
        return $orderservice->getOrder($blueLinkOrderId);
    }

    public function getOrders($filter = null)
    {
        if($filter == null)
        {
            return $this->shopify->Order->get();
        }
        else
        {
            if(!is_array($filter))
            {
                echo "Error: Filter must be of type Array".PHP_EOL;
                return false;
            }
            else
            {
                return $this->shopify->Order->get($filter);
            }
        }
    }

    public function addSalesOrderToBlueLink($orderId)
    {
        $orderService = $this->orderService();

        $order = $this->shopify->Order($orderId)->get();

        if($order['shipping_address']['province_code'] == 'FL')
        {
            $customerId = 'BrokeDickT';
        }
        else
        {
            $customerId = 'BrokeDick';
        }

        $customerShipTo = $this->addCustomerShipToAddress($order['id']);
        if($customerShipTo != true)
        {
            file_put_contents('ORDERERRORS.txt',date("Y-m-d H:i:s").": Customer ShipTo not added for order ".$order['name']."... Halting.\n",LOCK_EX | FILE_APPEND);
            return false;
        }

        $shipping_cost = 0;
        foreach($order['shipping_lines'] as $shipping_line)
            $shipping_cost += $shipping_line['price'];

        $blueLinkOrderId = $orderService->addSalesOrder(
            $customerId,
            'BD'.substr($order['customer']['default_address']['id'],-4),
            $shipping_cost,
            $order['name'],
            'Regular',
            'USPS Priority',
            'New');
        file_put_contents('ORDERLOGS.txt',date("Y-m-d H:i:s").": Order Header for ".$order['name']." created in BlueLink (".$blueLinkOrderId.")\n",LOCK_EX | FILE_APPEND);

        if(is_numeric($blueLinkOrderId)) {
            $order['reference'] = $blueLinkOrderId;
            $this->shopify->Order($orderId)->put($order);
        }

        $order_discount = $order['total_discounts'];
        $split_discount = $order_discount/count($order['line_items']);
        foreach($order['line_items'] as $line_item)
        {
            $description = $line_item['title'];
            if($line_item['variant_title'] != NULL)
                $description .= ' '.$line_item['variant_title'];

            if($line_item['price'] != 0 && $split_discount != 0)
                $discount_percent = ($split_discount/$line_item['price']);
            else
                $discount_percent = 0;
            $orderService->addSalesOrderLine($blueLinkOrderId,
                $line_item['sku'],
                $line_item['quantity'],
                $description,
                $line_item['price'],
                date('Y-m-d'),
                $discount_percent);
            file_put_contents('ORDERLOGS.txt',date("Y-m-d H:i:s").": (".$order['name'].") Order line for ".$line_item['sku']." created in BlueLink\n",LOCK_EX | FILE_APPEND);

        }

        return $blueLinkOrderId;
    }

    public function verifyWebhook($data, $hmac_header)
    {
        $calculated_hmac = base64_encode(hash_hmac('sha256',$data,$this->hookSecret,true));
        return ($hmac_header == $calculated_hmac);
    }
}
